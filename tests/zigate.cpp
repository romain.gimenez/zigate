/*
 * PiZiGate_CPP
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of PiZiGate_CPP.
 *
 * PiZiGate_CPP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiZiGate_CPP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiZiGate_CPP.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <thread>

#include "ZiGate/Gateway.h"
#include "ZiGate/Logger.h"
#include "ZiGate/Requests/GetDevicesList.h"
#include "ZiGate/Requests/GetNetworkState.h"
#include "ZiGate/Requests/GetVersion.h"
#include "ZiGate/Requests/SetChannelMask.h"
#include "ZiGate/Requests/SetDeviceType.h"
#include "ZiGate/Requests/StartNetwork.h"
#include "ZiGate/Transport/Usb.h"
#include "ZiGate/Util.h"

const std::string TEST_NAME = "ZiGate";

void printVersion(ZiGate::Gateway& zigate)
{
    ZiGate::Logger::info(TEST_NAME, "Getting version");

    auto getVersion = std::make_shared<ZiGate::Requests::GetVersion>();
    zigate.getInterface().send(getVersion);

    if (getVersion->waitForCompletion() != ZiGate::Requests::Base::Status::Completed)
    {
        ZiGate::Logger::error(TEST_NAME, "Failed to get version");
    }
}

void listDevices(ZiGate::Gateway& zigate)
{
    ZiGate::Logger::info(TEST_NAME, "Getting devices list");

    auto getDevicesList = std::make_shared<ZiGate::Requests::GetDevicesList>();
    zigate.getInterface().send(getDevicesList);

    if (getDevicesList->waitForCompletion() != ZiGate::Requests::Base::Status::Completed)
    {
        ZiGate::Logger::error(TEST_NAME, "Failed to get devices list");
    }
}

void handleDeviceAnnounce(const ZiGate::Responses::DeviceAnnounce& announce)
{
    ZiGate::Logger::info(TEST_NAME, "Got device announce: " + ZiGate::Util::uint16ToHexStr(announce.getShortAddress()));
}

void handleAttributeReport(const ZiGate::Responses::AttributeReport& report)
{
    ZiGate::Logger::info(TEST_NAME, "Got attribute report: " + ZiGate::Util::uint16ToHexStr(report.getShortAddress()));

    // General
    if (report.getCluster() == 0x0000u)
    {
        // Name
        if (report.getAttributeID() == 0x0005u)
        {
            std::string name {report.getAttributeData().begin(), report.getAttributeData().end()};
            ZiGate::Logger::info(TEST_NAME, "Name: " + name);
        }
            // Battery ?
        else if (report.getAttributeID() == 0xff01u)
        {
            float voltage = static_cast<float>(static_cast<std::uint16_t>(report.getAttributeData().at(3) << 8u) |
                                               static_cast<std::uint16_t>(report.getAttributeData().at(2))) / 1000.0f;

            ZiGate::Logger::info(TEST_NAME, "Voltage: " + std::to_string(voltage));

            float temperature = static_cast<float>(static_cast<std::uint16_t>(report.getAttributeData().at(22) << 8u) |
                                                   static_cast<std::uint16_t>(report.getAttributeData().at(21))) /
                                100.0f;

            ZiGate::Logger::info(TEST_NAME, "Temperature: " + std::to_string(temperature));

            float humidity = static_cast<float>(static_cast<std::uint16_t>(report.getAttributeData().at(26) << 8u) |
                                                static_cast<std::uint16_t>(report.getAttributeData().at(25))) / 100.0f;

            ZiGate::Logger::info(TEST_NAME, "Humidity: " + std::to_string(humidity));
        }
    }
        // Temperature
    else if (report.getCluster() == 0x0402u)
    {
        float temperature = static_cast<float>(static_cast<std::uint16_t>(report.getAttributeData().at(0) << 8u) |
                                               static_cast<std::uint16_t>(report.getAttributeData().at(1))) / 100.0f;
        ZiGate::Logger::info(TEST_NAME, "Temperature: " + std::to_string(temperature));
    }
        // Humidity
    else if (report.getCluster() == 0x0405u)
    {
        float humidity = static_cast<float>(static_cast<std::uint16_t>(report.getAttributeData().at(0) << 8u) |
                                            static_cast<std::uint16_t>(report.getAttributeData().at(1))) / 100.0f;
        ZiGate::Logger::info(TEST_NAME, "Humidity: " + std::to_string(humidity));
    }
}

int main(int, char**)
{
    // Starting the test
    ZiGate::Logger::info(TEST_NAME, "Starting test");
    auto start = std::chrono::system_clock::now();

    // Set log level to INFO
    //ZiGate::Logger::setLogLevel(boost::log::trivial::info);

    // Create ZiGate USB device
    auto zigate = std::make_unique<ZiGate::Gateway>(std::make_unique<ZiGate::Transport::Usb>("/dev/serial0"));
    ZiGate::Logger::info(TEST_NAME, "ZiGate transport info: " + zigate->getInterface().getInfo());

    // Connect to gateway signals
    auto deviceAnnounceConnection = zigate->getInterface().connectDeviceAnnounceSlot(&handleDeviceAnnounce);
    auto attributeReportConnection = zigate->getInterface().connectAttributeReportSlot(&handleAttributeReport);

    if (zigate->start())
    {
        // Print version
        printVersion(*zigate);

        // List available devices
        listDevices(*zigate);

        while (true)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }

    // Explicitly delete the device instance before test completion
    zigate.reset();

    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
    ZiGate::Logger::info(TEST_NAME, "Test completed in " + std::to_string(elapsed.count()));
    return 0;
}