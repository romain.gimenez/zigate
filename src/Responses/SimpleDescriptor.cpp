/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Logger.h"
#include "Message.h"
#include "Responses/SimpleDescriptor.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::SimpleDescriptor";
}

SimpleDescriptor::SimpleDescriptor(Message& message)
    : m_status(0u)
    , m_address(0u)
    , m_endpoint(0u)
    , m_profileID(0u)
    , m_deviceID(0u)
    , m_inputClusters()
    , m_outputClusters()
{
    auto sqn = message.read_uint8();
    m_status = message.read_uint8();
    m_address = message.read_uint16();
    auto length = message.read_uint8();
    m_endpoint = message.read_uint8();
    m_profileID = message.read_uint16();
    m_deviceID = message.read_uint16();
    auto inputCount = message.read_uint8();

    Logger::debug(LOG_SENDER, "Sequence number: " + std::to_string(sqn));
    Logger::debug(LOG_SENDER, "Status: " + std::to_string(m_status));
    Logger::debug(LOG_SENDER, "Address: " + Util::uint16ToHexStr(m_address));
    Logger::debug(LOG_SENDER, "Length: " + std::to_string(length));
    Logger::debug(LOG_SENDER, "Endpoint: " + Util::uint8ToHexStr(m_endpoint));
    Logger::debug(LOG_SENDER, "Profile ID: " + Util::uint16ToHexStr(m_profileID));
    Logger::debug(LOG_SENDER, "DeviceAnnounce ID: " + Util::uint16ToHexStr(m_deviceID));
    Logger::debug(LOG_SENDER, "Input clusters count: " + std::to_string(inputCount));

    for(std::uint8_t i = 0u; i < inputCount; ++i)
    {
        m_inputClusters.emplace_back(message.read_uint16());
        Logger::debug(LOG_SENDER, " - Input cluster: " + Util::uint16ToHexStr(m_inputClusters.back()));
    }

    auto outputCount = message.read_uint8();
    Logger::debug(LOG_SENDER, "Output clusters count: " + std::to_string(outputCount));

    for(std::uint8_t i = 0u; i < outputCount; ++i)
    {
        m_outputClusters.emplace_back(message.read_uint16());
        Logger::debug(LOG_SENDER, " - Output cluster: " + Util::uint16ToHexStr(m_outputClusters.back()));
    }
}

std::uint8_t SimpleDescriptor::getStatus() const
{
    return m_status;
}

std::uint16_t SimpleDescriptor::getAddress() const
{
    return m_address;
}

std::uint8_t SimpleDescriptor::getEndpoint() const
{
    return m_endpoint;
}

std::uint16_t SimpleDescriptor::getProfileID() const
{
    return m_profileID;
}

std::uint16_t SimpleDescriptor::getDeviceID() const
{
    return m_deviceID;
}

std::vector<std::uint16_t> SimpleDescriptor::getInputClusters() const
{
    return m_inputClusters;
}

std::vector<std::uint16_t> SimpleDescriptor::getOutputClusters() const
{
    return m_outputClusters;
}
