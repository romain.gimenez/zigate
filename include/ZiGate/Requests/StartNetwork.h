/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <mutex>

#include "../Responses/NetworkJoined.h"
#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to start the network of the gateway
     */
    class StartNetwork : public Base
    {
      public:

        /**
         * @brief Constructor
         */
        StartNetwork();

        /**
         * @brief Destructor
         */
        ~StartNetwork() override = default;

        /**
         * @brief Get the response to this request
         * @return The response to this request
         */
        [[nodiscard]] const Responses::NetworkJoined* getResponse() const;

        /**
         * @brief Handle response message from gateway
         * @param message The response message
         */
        void handleResponseMessage(Message& message) override;

      private:

        std::unique_ptr<Responses::NetworkJoined> m_response;
        mutable std::mutex m_mutex;
    };
}



