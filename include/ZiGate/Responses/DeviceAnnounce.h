/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class DeviceAnnounce
        {
          public:

            /**
             * @brief Constructor
             * @param message The announce message
             */
            explicit DeviceAnnounce(Message& message);

            /**
             * @brief Destructor
             */
            ~DeviceAnnounce() = default;

            /**
             * @brief Get the short address of the device
             * @return The short address of the device
             */
            [[nodiscard]] std::uint16_t getShortAddress() const;

            /**
             * @brief Get the IEEE address of the device
             * @return The IEEE address of the device
             */
            [[nodiscard]] std::uint64_t getIEEEAddress() const;

            /**
             * @brief Get the link quality of the device
             * @return The link quality of the device
             */
            [[nodiscard]] std::uint8_t getLinkQuality() const;


          private:

            std::uint16_t m_shortAddress;
            std::uint64_t m_ieeeAddress;
            std::uint8_t m_macCapability;
            std::uint8_t m_linkQuality;
        };
    }
}
