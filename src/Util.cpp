/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iomanip>
#include <ostream>

#include "Util.h"

using namespace ZiGate;

std::string Util::uint8ToHexStr(std::uint8_t value)
{
    std::ostringstream stream;

    stream << std::setfill('0') << std::setw(2u) << std::hex << static_cast<std::uint16_t>(value);
    return stream.str();
}

std::string Util::uint16ToHexStr(std::uint16_t value)
{
    std::ostringstream stream;

    stream << std::setfill('0') << std::setw(4u) << std::hex << value;
    return stream.str();
}

std::string Util::dataToHexStr(const ZiGate::data_t& value)
{
    std::ostringstream stream;

    for (const auto& byte : value)
    {
        stream << " " << std::setfill('0') << std::setw(2u) << std::hex << static_cast<std::uint16_t>(byte);
    }

    return stream.str();
}

std::uint16_t Util::hexStrToUint16(const std::string& value)
{
    std::uint16_t result;
    std::stringstream stream;
    stream << std::hex << value;
    stream >> result;
    return result;
}