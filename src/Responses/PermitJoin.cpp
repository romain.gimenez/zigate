/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Logger.h"
#include "Message.h"
#include "Responses/PermitJoin.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::PermitJoin";
}

PermitJoin::PermitJoin(ZiGate::Message& message)
    : m_state(false)
{
    m_state = message.read_bool();

    Logger::debug(LOG_SENDER, "State: " + std::to_string(m_state));
}

bool PermitJoin::getState() const
{
    return m_state;
}
