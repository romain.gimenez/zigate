/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Requests/ReadAttribute.h"
#include "Message.h"

using namespace ZiGate::Requests;

ReadAttribute::ReadAttribute(std::uint16_t shortAddress, std::uint8_t sourceEndpoint, std::uint8_t destinationEndpoint,
                             std::uint16_t cluster, ReadAttribute::Direction direction,
                             ReadAttribute::ManufacturerSpecific manufacturerSpecific, std::uint16_t manufacturer,
                             std::vector<std::uint16_t> attributes)
    : Base("ReadAttribute", MessageType::ReadAttribute, MessageType::AttributeRead)
    , m_shortAddress(shortAddress)
    , m_sourceEndpoint(sourceEndpoint)
    , m_destinationEndpoint(destinationEndpoint)
    , m_cluster(cluster)
    , m_direction(direction)
    , m_manufacturerSpecific(manufacturerSpecific)
    , m_manufacturer(manufacturer)
    , m_attributes(std::move(attributes))
{
    //ctor
}

ZiGate::Message ReadAttribute::generate() const
{
    Message msg {static_cast<std::uint16_t>(m_commandType)};

    msg.write_uint8(0x02u); // Address mode -> short address
    msg.write_uint16(m_shortAddress);
    msg.write_uint8(m_sourceEndpoint);
    msg.write_uint8(m_destinationEndpoint);
    msg.write_uint16(m_cluster);
    msg.write_uint8(static_cast<std::uint8_t>(m_direction));
    msg.write_uint8(static_cast<std::uint8_t>(m_manufacturerSpecific));
    if (m_manufacturerSpecific == ManufacturerSpecific::Yes)
    {
        msg.write_uint16(m_manufacturer);
    }
    msg.write_uint16(static_cast<std::uint16_t>(m_attributes.size()));
    for(const auto& attribute : m_attributes)
    {
        msg.write_uint16(attribute);
    }

    return msg;
}
