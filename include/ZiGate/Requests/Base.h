/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <atomic>
#include <string>

#include "../MessageType.h"

namespace ZiGate
{
    class Message;

    namespace Requests
    {
        /**
         * @brief Base class for all other requests
         */
        class Base
        {
          public:

            /**
             * @brief Represents the status of a request
             */
            enum class Status
            {
                Created,
                Received,
                Completed,
                Failed,
                Timeout
            };

            /**
             * @brief Destructor
             */
            virtual ~Base() = default;

            /**
             * @brief Get the name fo the request
             * @return The name of the request
             */
            [[nodiscard]] std::string getName() const;

            /**
             * @brief Get the status of the request
             * @return The status of the request
             */
            [[nodiscard]] Status getStatus() const;

            /**
             * @brief Get the command associated with this request
             * @return The command associated with this request
             */
            [[nodiscard]] MessageType getCommandType() const;

            /**
             * @brief Get the ID of the response associated with this request (0x0000u if none)
             * @return The ID of the response associated with this request (0x0000u if none)
             */
            [[nodiscard]] MessageType getResponseType() const;

            /**
             * @brief Wait for the request to be processed by the gateway
             * @return The Status of the request @see Status
             */
            [[nodiscard]] Status waitForCompletion();

            /**
             * @brief Generate the message to send for this request
             * @return The message to send to the gateway
             */
            [[nodiscard]] virtual Message generate() const;

            /**
             * @brief Handle a status code from the gateway
             * @param code The status code received from the gateway
             */
            void handleStatus(std::uint8_t code);

            /**
             * @brief Handle a response message from the gateway
             * @param response The response message received from the gateway
             */
            virtual void handleResponseMessage(Message& response);

          protected:

            /**
             * @brief Constructor
             * @param name The name of the request
             * @param commandType The command of the request
             * @param responseType The response associated with the request (0x0000u if none)
             */
            Base(std::string name, MessageType commandType, MessageType responseType);

            std::string m_name;
            std::atomic<Status> m_status;
            MessageType m_commandType;
            MessageType m_responseType;
        };
    }
}



