/*
 * PiZiGate_CPP
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of PiZiGate_CPP.
 *
 * PiZiGate_CPP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiZiGate_CPP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiZiGate_CPP.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <thread>

#include "ZiGate/Gateway.h"
#include "ZiGate/Logger.h"
#include "ZiGate/Requests/ActionOnOff.h"
#include "ZiGate/Requests/GetNetworkState.h"
#include "ZiGate/Requests/SetDeviceType.h"
#include "ZiGate/Requests/StartNetwork.h"
#include "ZiGate/Transport/Usb.h"
#include "ZiGate/Util.h"

const std::string TEST_NAME = "SwitchOnOff";

int main(int, char**)
{
    // Starting the test
    ZiGate::Logger::info(TEST_NAME, "Starting test");
    auto start = std::chrono::system_clock::now();

    // Set log level to INFO
    //ZiGate::Logger::setLogLevel(boost::log::trivial::info);

    // Create ZiGate USB device
    auto zigate = std::make_unique<ZiGate::Gateway>(std::make_unique<ZiGate::Transport::Usb>("/dev/serial0"));
    ZiGate::Logger::info(TEST_NAME, "ZiGate transport info: " + zigate->getInterface().getInfo());

    if (zigate->start())
    {
        auto on = std::make_shared<ZiGate::Requests::ActionOnOff>(0xf610u,
                                                                  0x01u,
                                                                  0x01u,
                                                                  ZiGate::Requests::ActionOnOff::Type::On);

        auto off = std::make_shared<ZiGate::Requests::ActionOnOff>(0xf610u,
                                                                   0x01u,
                                                                   0x01u,
                                                                   ZiGate::Requests::ActionOnOff::Type::Off);


       /* zigate->getInterface().send(on);

        std::this_thread::sleep_for(std::chrono::seconds(5));*/

        zigate->getInterface().send(off);

        /*std::this_thread::sleep_for(std::chrono::seconds(5));*/
    }

    // Explicitly delete the device instance before test completion
    zigate.reset();

    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
    ZiGate::Logger::info(TEST_NAME, "Test completed in " + std::to_string(elapsed.count()));
    return 0;
}