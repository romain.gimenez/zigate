/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */


#pragma once

#include <boost/log/trivial.hpp>
#include <string>

namespace ZiGate::Logger
{
    /**
     * @brief Set the log level (DEFAULT: trace)
     */
    void setLogLevel(boost::log::trivial::severity_level level);

    /**
     * @brief Log a trace message
     * @param sender The sender of the message
     * @param message The message to log
     */
    void trace(const std::string& sender, const std::string& message);

    /**
     * @brief Log a debug message
     * @param sender The sender of the message
     * @param message The message to log
     */
    void debug(const std::string& sender, const std::string& message);

    /**
     * @brief Log an info message
     * @param sender The sender of the message
     * @param message The message to log
     */
    void info(const std::string& sender, const std::string& message);

    /**
     * @brief Log a warning message
     * @param sender The sender of the message
     * @param message The message to log
     */
    void warning(const std::string& sender, const std::string& message);

    /**
     * @brief Log an error message
     * @param sender The sender of the message
     * @param message The message to log
     */
    void error(const std::string& sender, const std::string& message);

    /**
     * @brief Log a fatal message
     * @param sender The sender of the message
     * @param message The message to log
     */
    void fatal(const std::string& sender, const std::string& message);
}


