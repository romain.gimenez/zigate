/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <unordered_map>

#include "../Devices/Info.h"

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class DevicesList
        {
          public:

            typedef std::unordered_map<std::uint8_t, Devices::Info> List_t;

            /**
             * @brief Constructor
             * @param Message The message to parse for creating this response
             */
            explicit DevicesList(Message& message);

            /**
             * @brief Destructor
             */
            ~DevicesList() = default;

            /**
             * @brief Get the list of devices
             * @return The list of devices
             */
            [[nodiscard]] const List_t& getDevices() const;

          private:

            List_t m_list;
        };
    }
}
