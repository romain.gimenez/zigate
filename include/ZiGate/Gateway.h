/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Transport/Interface.h"

namespace ZiGate
{
    /**
     * @brief Represents the ZIGate gateway
     */
    class Gateway
    {
      public:

        /**
         * @brief Constructor
         * @param interface The transport interface of the gateway
         */
        explicit Gateway(std::unique_ptr<Transport::Interface> interface);

        /**
         * @brief Destructor
         */
        ~Gateway() = default;

        /**
         * @brief Start the gateway
         * @return true for success, false otherwise
         */
        bool start();

        /**
         * @brief Access the transport interface of the gateway
         * @return A reference to the transport interface
         */
        Transport::Interface& getInterface();

      private:

        std::unique_ptr<Transport::Interface> m_interface;
    };
}



