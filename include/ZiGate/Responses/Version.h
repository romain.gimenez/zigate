/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class Version
        {
          public:

            /**
             * @brief Constructor
             * @param message The message to parse for creating this response
             */
            explicit Version(Message& message);

            /**
             * @brief Destructor
             */
            ~Version() = default;

            /**
             * @brief Get the major version
             * @return The major version
             */
            [[nodiscard]] std::uint16_t getMajor() const;

            /**
             * @brief Get the installer version
             * @return The installer version
             */
            [[nodiscard]] std::uint16_t getInstaller() const;

          private:

            std::uint16_t m_major;
            std::uint16_t m_installer;
        };
    }
}
