/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Logger.h"
#include "Message.h"
#include "Responses/Version.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::Version";
}

Version::Version(ZiGate::Message& message)
    : m_major(0u)
    , m_installer(0u)
{
    m_major = message.read_uint16();
    m_installer = message.read_uint16();

    Logger::debug(LOG_SENDER, "Major: " + Util::uint16ToHexStr(m_major));
    Logger::debug(LOG_SENDER, "Installer: " + Util::uint16ToHexStr(m_installer));
}

std::uint16_t Version::getMajor() const
{
    return m_major;
}

std::uint16_t Version::getInstaller() const
{
    return m_installer;
}
