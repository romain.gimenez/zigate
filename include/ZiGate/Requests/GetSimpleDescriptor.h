/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <mutex>

#include "../Responses/SimpleDescriptor.h"
#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to get a simple descriptor of a device endpoint from the gateway
     */
    class GetSimpleDescriptor : public Base
    {
      public:

        /**
         * @brief Constructor
         * @param shortAddress The short address of the device
         * @param endpoint The end point of the device
         */
        GetSimpleDescriptor(std::uint16_t shortAddress, std::uint8_t endpoint);

        /**
         * @brief Destructor
         */
        ~GetSimpleDescriptor() override = default;

        /**
         * @brief Get the response to this request
         * @return The response to this request
         */
        [[nodiscard]] const Responses::SimpleDescriptor* getResponse() const;

        /**
         * @brief Generate the message to send for this request
         * @return The message to send to the gateway
         */
        [[nodiscard]] Message generate() const override;

        /**
         * @brief Handle response message from gateway
         * @param message The response message
         */
        void handleResponseMessage(Message& message) override;

      private:

        std::uint16_t m_shortAddress;
        std::uint8_t m_endpoint;
        std::unique_ptr<Responses::SimpleDescriptor> m_response;
        mutable std::mutex m_mutex;
    };
}



