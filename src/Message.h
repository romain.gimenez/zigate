/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <vector>

#include "Constants.h"
#include "MessageType.h"
#include "Typedefs.h"

namespace ZiGate
{
    namespace Transport
    {
        class Interface;
    }

    class Message
    {
      public:

        /**
         * @brief Constructor
         * @param command The command type of the message
         */
        explicit Message(std::uint16_t command);

        /**
         * @brief Constructor
         * @param command The command type of the message
         * @param data The payload of the message
         */
        Message(std::uint16_t command, data_t data);

        /**
         * @brief Decode a message from encoded data from the gateway
         * @param data The encoded data
         * @return The decoded message
         */
        static Message decode(const data_t& data);

        /**
         * @brief Destructor
         */
        ~Message() = default;

        /**
         * @brief Get the type of the message
         * @return The type of the message
         */
        [[nodiscard]] std::uint16_t getType() const;

        /**
         * @brief Get the data length
         * @return The data length
         */
        [[nodiscard]] std::uint16_t getDataLength() const;

        /**
         * @brief Encode the message before sending it to the gateway
         * @return The encoded data
         */
        [[nodiscard]] data_t encode() const;

        /**
         * @brief Reset the read index
         */
        void resetRead();

        /**
         * @brief Read an uint8 from the data buffer
         * @return The value from the data buffer
         */
        std::uint8_t read_uint8();

        /**
         * @brief Read an uint16 from the data buffer
         * @return The value from the data buffer
         */
        std::uint16_t read_uint16();

        /**
         * @brief Read an uint32 from the data buffer
         * @return The value from the data buffer
         */
        std::uint32_t read_uint32();

        /**
         * @brief Read an uint64 from the data buffer
         * @return The value from the data buffer
         */
        std::uint64_t read_uint64();

        /**
         * @brief Red a bool from the data buffer
         * @return The value from the data buffer
         */
        bool read_bool();

        /**
         * @brief Read a string from the data buffer
         * @param size The size of the string to read
         * @return The value from the data buffer
         */
        std::string read_string(std::uint16_t size);

        /**
         * @brief Read raw data from the data buffer
         * @param size The size of the data to read
         * @return The value from the data buffer
         */
        data_t read_data(std::uint16_t size);

        /**
         * @brief Write a uint8 value into the data buffer
         * @param value The value to write
         */
        void write_uint8(std::uint8_t value);

        /**
         * @brief Write a uint16 value into the data buffer
         * @param value The value to write
         */
        void write_uint16(std::uint16_t value);

        /**
         * @brief Write a uint32 value into the data buffer
         * @param value The value to write
         */
        void write_uint32(std::uint32_t value);

        /**
         * @brief Write a uint8 value into the data buffer
         * @param value The value to write
         */
        void write_uint64(std::uint64_t value);

        /**
         * @brief Write a bool value into the data buffer
         * @param value The value to write
         */
        void write_bool(bool value);

        /**
         * @brief Write raw data to the data buffer
         * @param value The value to write
         */
        void write_data(const data_t& value);

      private:

        /**
         * @brief Calculate the checksum of a message
         * @param command The command type of the message
         * @param dataLength The length of the payload data
         * @param data The payload data
         * @return The calculated checksum
         */
        static std::uint8_t calculateChecksum(std::uint16_t command, std::uint16_t dataLength, const data_t& data);

        std::uint16_t m_command;
        data_t m_data;
        std::uint16_t m_readIndex;
    };
}



