/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Logger.h"
#include "Message.h"
#include "Responses/NetworkJoined.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::NetworkJoined";
}

NetworkJoined::NetworkJoined(ZiGate::Message& message)
    : m_status(0u)
    , m_shortAddress(0u)
    , m_extendedAddress(0u)
    , m_channel(0u)
{
    m_status = message.read_uint8();
    m_shortAddress = message.read_uint16();
    m_extendedAddress = message.read_uint64();
    m_channel = message.read_uint8();

    Logger::debug(LOG_SENDER, "Status: " + std::to_string(m_status));
    Logger::debug(LOG_SENDER, "Short address: " + Util::uint16ToHexStr(m_shortAddress));
    Logger::debug(LOG_SENDER, "Extended address: " + std::to_string(m_extendedAddress));
    Logger::debug(LOG_SENDER, "Channel: " + std::to_string(m_channel));
}

std::uint8_t NetworkJoined::getStatus() const
{
    return m_status;
}

std::uint16_t NetworkJoined::getShortAddress() const
{
    return m_shortAddress;
}

std::uint64_t NetworkJoined::getExtendedAddress() const
{
    return m_extendedAddress;
}

std::uint8_t NetworkJoined::getChannel() const
{
    return m_channel;
}

