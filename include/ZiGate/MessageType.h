/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>

namespace ZiGate
{
    enum class MessageType : std::uint16_t
    {
        Null = 0x0000u,

        // Requests
        GetNetworkState = 0x0009u,
        GetVersion = 0x0010u,
        GetPermitJoin = 0x0014u,
        GetDevicesList = 0x0015u,
        SetTimeServer = 0x0016u,
        GetTimeServer = 0x0017u,
        SetLed = 0x0018u,
        SetCertification = 0x0019u,
        SetExtendedPanID = 0x0020u,
        SetChannelMask = 0x0021u,
        SetSecurityKey = 0x0022u,
        SetDeviceType = 0x0023u,
        StartNetwork = 0x0024u,
        StartNetworkScan = 0x0025u,
        RemoveDevice = 0x0026u,
        GetNodeDescriptor = 0x0042u,
        GetSimpleDescriptor = 0x0043u,
        GetPowerDescriptor = 0x0044u,
        GetActiveEndpoints = 0x0045u,
        DeviceAnnounce = 0x004Du,
        ActionOnOff = 0x0092u,
        ReadAttribute = 0x0100u,
        Status = 0x8000u,
        NetworkState = 0x8009u,
        Version = 0x8010u,
        PermitJoinStatus = 0x8014,
        DevicesList = 0x8015u,
        TimeServer = 0x8017u,
        NetworkJoined = 0x8024u,
        NodeDescriptor = 0x8042u,
        SimpleDescriptor = 0x8043u,
        PowerDescriptor = 0x8044u,
        ActiveEndpoints = 0x8045u,
        AttributeRead = 0x8100u,
        AttributeReport = 0x8102,
        RouterDiscoveryConfirm = 0x8701,
        APSDataConfirmFail = 0x8702,
    };
}


