/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to switch ON/OFF a device from the gateway
     */
    class ActionOnOff : public Base
    {
      public:

        enum class Type : std::uint8_t
        {
            Off = 0x00u,
            On = 0x01u,
            Toggle = 0x02u
        };

        /**
         * @brief Constructor
         * @param shortAddress The short address
         * @param sourceEndpoint The source endpoint
         * @param destinationEndpoint The destination endpoint
         * @param type The type of the action
         */
        explicit ActionOnOff(std::uint16_t shortAddress, std::uint8_t sourceEndpoint, std::uint8_t destinationEndpoint,
                             Type type);

        /**
         * @brief Destructor
         */
        ~ActionOnOff() override = default;

        /**
         * @brief Generate the message to send for this request
         * @return The message to send to the gateway
         */
        [[nodiscard]] Message generate() const override;

      private:

        std::uint16_t m_shortAddress;
        std::uint8_t m_sourceEndpoint;
        std::uint8_t m_destinationEndpoint;
        Type m_type;
    };
}



