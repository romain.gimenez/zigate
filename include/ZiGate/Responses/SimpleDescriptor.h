/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class SimpleDescriptor
        {
          public:

            /**
             * @brief Constructor
             * @param message The message to parse for creating this response
             */
            explicit SimpleDescriptor(Message& message);

            /**
             * @brief Destructor
             */
            ~SimpleDescriptor() = default;

            /**
             * @brief Get the status
             * @return The status
             */
            [[nodiscard]] std::uint8_t getStatus() const;

            /**
             * @brief Get the network address
             * @return The network address
             */
            [[nodiscard]] std::uint16_t getAddress() const;

            /**
             * @brief Get the endpoint ID
             * @return The endpoint ID
             */
            [[nodiscard]] std::uint8_t getEndpoint() const;

            /**
             * @brief Get the profile ID
             * @return The profile ID
             */
            [[nodiscard]] std::uint16_t getProfileID() const;

            /**
             * @brief Get the device ID
             * @return The device ID
             */
            [[nodiscard]] std::uint16_t getDeviceID() const;

            /**
             * @brief Get the input clusters
             * @return The input clusters
             */
            [[nodiscard]] std::vector<std::uint16_t> getInputClusters() const;

            /**
             * @brief Get the output clusters
             * @return The output clusters
             */
            [[nodiscard]] std::vector<std::uint16_t> getOutputClusters() const;

          private:

            std::uint8_t m_status;
            std::uint16_t m_address;
            std::uint8_t m_endpoint;
            std::uint16_t m_profileID;
            std::uint16_t m_deviceID;
            std::vector <std::uint16_t> m_inputClusters;
            std::vector <std::uint16_t> m_outputClusters;
        };
    }
}
