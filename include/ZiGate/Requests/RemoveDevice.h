/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to remove a device from the gateway
     */
    class RemoveDevice : public Base
    {
      public:

        /**
         * @brief Constructor
         * @param ieeeAddress The IEEE address of the device to remove
         */
        explicit RemoveDevice(std::uint64_t ieeeAddress);

        /**
         * @brief Destructor
         */
        ~RemoveDevice() override = default;

        /**
         * @brief Generate the message to send for this request
         * @return The message to send to the gateway
         */
        [[nodiscard]] Message generate() const override;

      private:

        std::uint64_t m_ieeeAddress;
    };
}



