/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Gateway.h"
#include "Logger.h"
#include "Requests/GetNetworkState.h"
#include "Requests/SetDeviceType.h"
#include "Requests/StartNetwork.h"

using namespace ZiGate;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Gateway";
}

Gateway::Gateway(std::unique_ptr<Transport::Interface> interface)
    : m_interface(std::move(interface))
{
    //ctor
}

bool Gateway::start()
{
    ZiGate::Logger::debug(LOG_SENDER, "Starting up");

    // Open the communication
    ZiGate::Logger::debug(LOG_SENDER, "Opening communication");
    auto ec = m_interface->open();
    if (ec)
    {
        ZiGate::Logger::error(LOG_SENDER, "Failed to open communication");
        return false;
    }

    // Set channel TODO ?
    /*ZiGate::Logger::debug(LOG_SENDER, "Setting channel mask");
    auto setChannel = std::make_shared<ZiGate::Requests::SetChannelMask>();
    zigate->getInterface().send(setChannel);
    if (setChannel->waitForCompletion() != ZiGate::Requests::Base::Status::Completed)
    {
        ZiGate::Logger::error(TEST_NAME, "Failed to set channel mask");
        return false;
    }*/

    // Set device type
    ZiGate::Logger::debug(LOG_SENDER, "Setting device type");
    auto setDeviceType = std::make_shared<ZiGate::Requests::SetDeviceType>(
        ZiGate::Requests::SetDeviceType::DeviceType::Coordinator);
    m_interface->send(setDeviceType);
    if (setDeviceType->waitForCompletion() != ZiGate::Requests::Base::Status::Completed)
    {
        ZiGate::Logger::error(LOG_SENDER, "Failed to set device type");
        return false;
    }

    // Check network state
    ZiGate::Logger::debug(LOG_SENDER, "Checking network state");
    auto getNetworkState = std::make_shared<ZiGate::Requests::GetNetworkState>();
    m_interface->send(getNetworkState);
    if (getNetworkState->waitForCompletion() != ZiGate::Requests::Base::Status::Completed)
    {
        ZiGate::Logger::error(LOG_SENDER, "Failed to get network state");
        return false;
    }
    else
    {
        if (getNetworkState->getResponse()->getShortAddress() == 0xffffu ||
            getNetworkState->getResponse()->getExtendedPanID() == 0u)
        {
            ZiGate::Logger::debug(LOG_SENDER, "Network is down, starting it");

            // Start network
            auto startNetwork = std::make_shared<ZiGate::Requests::StartNetwork>();
            m_interface->send(startNetwork);
            if (startNetwork->waitForCompletion() != ZiGate::Requests::Base::Status::Completed)
            {
                ZiGate::Logger::error(LOG_SENDER, "Failed to start network");
                return false;
            }
        }
    }

    ZiGate::Logger::debug(LOG_SENDER, "Startup completed");
    return true;
}

Transport::Interface& Gateway::getInterface()
{
    return *m_interface;
}
