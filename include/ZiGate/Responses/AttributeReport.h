/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "../Typedefs.h"

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class AttributeReport
        {
          public:

            /**
             * @brief Constructor
             * @param message The announce message
             */
            explicit AttributeReport(Message& message);

            /**
             * @brief Destructor
             */
            ~AttributeReport() = default;

            /**
             * @brief Get the sequence number
             * @return The sequence number
             */
            [[nodiscard]] std::uint8_t getSequenceNumber() const;

            /**
             * @brief Get the short address of the device
             * @return The short address of the device
             */
            [[nodiscard]] std::uint16_t getShortAddress() const;

            /**
             * @brief Get the endpoint of the device
             * @return The endpoint of the device
             */
            [[nodiscard]] std::uint8_t getEndpoint() const;

            /**
             * @brief Get the cluster of the endpoint
             * @return The cluster of the endpoint
             */
            [[nodiscard]] std::uint16_t getCluster() const;

            /**
             * @brief Get the attribute ID
             * @return The attribute ID
             */
            [[nodiscard]] std::uint16_t getAttributeID() const;

            /**
             * @brief Get the attribute size
             * @return The attribute size
             */
            [[nodiscard]] std::uint16_t getAttributeSize() const;

            /**
             * @brief Get the attribute type
             * @return The attribute type
             */
            [[nodiscard]] std::uint16_t getAttributeType() const;

            /**
             * @brief Get the attribute data
             * @return The attribute data
             */
            [[nodiscard]] const data_t& getAttributeData() const;

            /**
             * @brief Get the status
             * @return The status
             */
            [[nodiscard]] std::uint8_t getStatus() const;


          private:

            std::uint8_t m_sequenceNumber;
            std::uint16_t m_shortAddress;
            std::uint8_t m_endpoint;
            std::uint16_t m_cluster;
            std::uint16_t m_attributeID;
            std::uint16_t m_attributeSize;
            std::uint16_t m_attributeType;
            data_t m_attributeData;
            std::uint8_t m_status;
        };
    }
}
