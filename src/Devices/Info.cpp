/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Devices/Info.h"

using namespace ZiGate::Devices;

Info::Info(std::uint8_t id, std::uint16_t shortAddress, std::uint64_t ieeeAddress, bool acPower,
           std::uint8_t linkQuality)
    : m_id(id)
    , m_shortAddress(shortAddress)
    , m_ieeeAddress(ieeeAddress)
    , m_acPower(acPower)
    , m_linkQuality(linkQuality)
{

}

std::uint8_t Info::getID() const
{
    return m_id;
}

std::uint16_t Info::getShortAddress() const
{
    return m_shortAddress;
}

std::uint64_t Info::getIEEEAddress() const
{
    return m_ieeeAddress;
}

bool Info::hasACPower() const
{
    return m_acPower;
}

std::uint8_t Info::getLinkQuality() const
{
    return m_linkQuality;
}
