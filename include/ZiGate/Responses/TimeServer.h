/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class TimeServer
        {
          public:

            /**
             * @brief Constructor
             * @param message The message to parse for creating this response
             */
            explicit TimeServer(Message& message);

            /**
             * @brief Destructor
             */
            ~TimeServer() = default;

            /**
             * @brief Get timestamp
             * @return Timestamp from 2000-01-01 00:00:00
             */
            [[nodiscard]] std::uint32_t getTimestamp() const;

          private:

            std::uint32_t m_timestamp;
        };
    }
}
