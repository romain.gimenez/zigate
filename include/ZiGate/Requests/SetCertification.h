/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to set the certification of the gateway
     */
    class SetCertification : public Base
    {
      public:

        /**
         * @brief The different types of certification
         */
        enum class Certification : std::uint8_t
        {
            CE = 1u,
            FCC = 2u,
        };

        /**
         * @brief Constructor
         * @param certification The certification to set @see Certification
         */
        explicit SetCertification(Certification certification);

        /**
         * @brief Destructor
         */
        ~SetCertification() override = default;

        /**
         * @brief Generate the message to send for this request
         * @return The message to send to the gateway
         */
        [[nodiscard]] Message generate() const override;

      private:

        Certification m_certification;
    };
}



