/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Responses/AttributeReport.h"
#include "Logger.h"
#include "Message.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::AttributeReport";
}

AttributeReport::AttributeReport(ZiGate::Message& message)
    : m_sequenceNumber(0u)
    , m_shortAddress(0u)
    , m_endpoint(0u)
    , m_cluster(0u)
    , m_attributeID(0u)
    , m_attributeSize(0u)
    , m_attributeType(0u)
    , m_attributeData()
    , m_status(0u)
{
    m_sequenceNumber = message.read_uint8();
    m_shortAddress = message.read_uint16();
    m_endpoint = message.read_uint8();
    m_cluster = message.read_uint16();
    m_attributeID = message.read_uint16();
    m_attributeType = message.read_uint16();
    m_attributeSize = message.read_uint16();
    m_attributeData = message.read_data(m_attributeSize );
    m_status = message.read_uint8();

    Logger::debug(LOG_SENDER, "SQN: " + std::to_string(m_sequenceNumber));
    Logger::debug(LOG_SENDER, "Short address: " + Util::uint16ToHexStr(m_shortAddress));
    Logger::debug(LOG_SENDER, "Endpoint: " + Util::uint8ToHexStr(m_endpoint));
    Logger::debug(LOG_SENDER, "Cluster ID: " + Util::uint16ToHexStr(m_cluster));
    Logger::debug(LOG_SENDER, "Attribute ID: " + Util::uint16ToHexStr(m_attributeID));
    Logger::debug(LOG_SENDER, "Attribute type: " + Util::uint16ToHexStr(m_attributeType));
    Logger::debug(LOG_SENDER, "Attribute size: " + std::to_string(m_attributeSize));
    Logger::debug(LOG_SENDER, "Attribute data: " + Util::dataToHexStr(m_attributeData));
    Logger::debug(LOG_SENDER, "Status: " + std::to_string(m_status));
}

std::uint8_t AttributeReport::getSequenceNumber() const
{
    return m_sequenceNumber;
}

std::uint16_t AttributeReport::getShortAddress() const
{
    return m_shortAddress;
}

std::uint8_t AttributeReport::getEndpoint() const
{
    return m_endpoint;
}

std::uint16_t AttributeReport::getCluster() const
{
    return m_cluster;
}

std::uint16_t AttributeReport::getAttributeID() const
{
    return m_attributeID;
}

std::uint16_t AttributeReport::getAttributeType() const
{
    return m_attributeType;
}

std::uint16_t AttributeReport::getAttributeSize() const
{
    return m_attributeSize;
}

const ZiGate::data_t& AttributeReport::getAttributeData() const
{
    return m_attributeData;
}

std::uint8_t AttributeReport::getStatus() const
{
    return m_status;
}
