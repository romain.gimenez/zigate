/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to set the device type of the gateway
     */
    class SetDeviceType : public Base
    {
      public:

        /**
         * @brief The different types of device
         */
        enum class DeviceType : std::uint8_t
        {
            Coordinator = 0u,
            Router = 1u,
        };

        /**
         * @brief Constructor
         * @param deviceType The device type to set @see DeviceType
         */
        explicit SetDeviceType(DeviceType deviceType);

        /**
         * @brief Destructor
         */
        ~SetDeviceType() override = default;

        /**
         * @brief Generate the message to send for this request
         * @return The message to send to the gateway
         */
        [[nodiscard]] Message generate() const override;

      private:

        DeviceType m_deviceType;
    };
}



