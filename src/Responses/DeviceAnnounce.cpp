/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Responses/DeviceAnnounce.h"
#include "Logger.h"
#include "Message.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::DeviceAnnounce";
}

DeviceAnnounce::DeviceAnnounce(ZiGate::Message& message)
    : m_shortAddress(0u)
    , m_ieeeAddress(0u)
    , m_macCapability(0u)
    , m_linkQuality(0u)
{
    m_shortAddress = message.read_uint16();
    m_ieeeAddress = message.read_uint64();
    m_macCapability = message.read_uint8();
    m_linkQuality = message.read_uint8();

    Logger::debug(LOG_SENDER, "Short address: " + Util::uint16ToHexStr(m_shortAddress));
    Logger::debug(LOG_SENDER, "IEEE address: " + std::to_string(m_ieeeAddress));
    Logger::debug(LOG_SENDER, "MAC capability: " + Util::uint8ToHexStr(m_macCapability));
    Logger::debug(LOG_SENDER, "Link quality: " + std::to_string(m_linkQuality));
}

std::uint16_t DeviceAnnounce::getShortAddress() const
{
    return m_shortAddress;
}

std::uint64_t DeviceAnnounce::getIEEEAddress() const
{
    return m_ieeeAddress;
}

std::uint8_t DeviceAnnounce::getLinkQuality() const
{
    return m_linkQuality;
}
