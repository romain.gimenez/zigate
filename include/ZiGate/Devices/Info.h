/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>

namespace ZiGate
{
    namespace Devices
    {
        /**
         * @brief Information about a device
         */
        class Info
        {
          public:

            /**
             * @brief Constructor
             * @param id The ID of the device
             * @param shortAddress The short address of the device
             * @param ieeeAddress The IEEE address of the device
             * @param acPower true if device is powered by AC, false for battery)
             * @param linkQuality The link quality withe the device
             */
            Info(std::uint8_t id, std::uint16_t shortAddress, std::uint64_t ieeeAddress, bool acPower,
                 std::uint8_t linkQuality);

            /**
             * @brief Destructor
             */
            ~Info() = default;

            /**
             * @brief Get the ID of the device
             * @return The ID of the device
             */
            [[nodiscard]] std::uint8_t getID() const;

            /**
             * @brief Get the short address of the device
             * @return The short address of the device
             */
            [[nodiscard]] std::uint16_t getShortAddress() const;

            /**
             * @brief Get the IEEE address of the device
             * @return The IEEE address of the device
             */
            [[nodiscard]] std::uint64_t getIEEEAddress() const;

            /**
             * @brief Get the power source of the device
             * @return true for AC power, false for battery
             */
            [[nodiscard]] bool hasACPower() const;

            /**
             * @brief Get the link quality of the device
             * @return The link quality of the device
             */
            [[nodiscard]] std::uint8_t getLinkQuality() const;

          private:

            std::uint8_t m_id;
            std::uint16_t m_shortAddress;
            std::uint64_t m_ieeeAddress;
            bool m_acPower;
            std::uint8_t m_linkQuality;
        };
    }
}
