/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

#include "Base.h"

namespace ZiGate::Requests
{
    /**
     * @brief Represents a request to read an attribute from a device
     */
    class ReadAttribute : public Base
    {
      public:

        enum class Direction : std::uint8_t
        {
            ServerToClient = 0x00u,
            ClientToServer = 0x01u,
        };

        enum class ManufacturerSpecific : std::uint8_t
        {
            No = 0x00u,
            Yes = 0x01u,
        };

        /**
         * @brief Constructor
         * @param shortAddress The short address
         * @param sourceEndpoint The source endpoint
         * @param destinationEndpoint The destination endpoint
         * @param cluster The cluster that contains the attributes
         * @param direction @see Direction
         * @param manufacturerSpecific @see ManufacturerSpecific
         * @param manufacturer The manufacturer ID (if ManufacturerSpecific::Yes)
         * @param attributes The list of attributes to read
         */
        explicit ReadAttribute(std::uint16_t shortAddress, std::uint8_t sourceEndpoint,
                               std::uint8_t destinationEndpoint, std::uint16_t cluster, Direction direction,
                               ManufacturerSpecific manufacturerSpecific, std::uint16_t manufacturer,
                               std::vector<std::uint16_t> attributes);

        /**
         * @brief Destructor
         */
        ~ReadAttribute() override = default;

        /**
         * @brief Generate the message to send for this request
         * @return The message to send to the gateway
         */
        [[nodiscard]] Message generate() const override;

      private:

        std::uint16_t m_shortAddress;
        std::uint8_t m_sourceEndpoint;
        std::uint8_t m_destinationEndpoint;
        std::uint16_t m_cluster;
        Direction m_direction;
        ManufacturerSpecific m_manufacturerSpecific;
        std::uint16_t m_manufacturer;
        std::vector<std::uint16_t> m_attributes;
    };
}



