/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Requests/ActionOnOff.h"
#include "Message.h"

using namespace ZiGate::Requests;

ActionOnOff::ActionOnOff(std::uint16_t shortAddress, std::uint8_t sourceEndpoint, std::uint8_t destinationEndpoint,
                         Type type)
    : Base("ActionOnOff", MessageType::ActionOnOff, MessageType::Null)
    , m_shortAddress(shortAddress)
    , m_sourceEndpoint(sourceEndpoint)
    , m_destinationEndpoint(destinationEndpoint)
    , m_type(type)
{
    //ctor
}

ZiGate::Message ActionOnOff::generate() const
{
    Message msg {static_cast<std::uint16_t>(m_commandType)};

    msg.write_uint8(0x02u); // Address mode -> short address
    msg.write_uint16(m_shortAddress);
    msg.write_uint8(m_sourceEndpoint);
    msg.write_uint8(m_destinationEndpoint);
    msg.write_uint8(static_cast<std::uint8_t>(m_type));

    return msg;
}
