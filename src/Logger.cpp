/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>

#include "Logger.h"

using namespace ZiGate;

void Logger::setLogLevel(boost::log::trivial::severity_level level)
{
    boost::log::core::get()->set_filter(boost::log::trivial::severity >= level);
}

void Logger::trace(const std::string& sender, const std::string& message)
{
    BOOST_LOG_TRIVIAL(trace) << "[" << sender << "] " << message;
}

void Logger::debug(const std::string& sender, const std::string& message)
{
    BOOST_LOG_TRIVIAL(debug) << "[" << sender << "] " << message;
}

void Logger::info(const std::string& sender, const std::string& message)
{
    BOOST_LOG_TRIVIAL(info) << "[" << sender << "] " << message;
}

void Logger::warning(const std::string& sender, const std::string& message)
{
    BOOST_LOG_TRIVIAL(warning) << "[" << sender << "] " << message;
}

void Logger::error(const std::string& sender, const std::string& message)
{
    BOOST_LOG_TRIVIAL(error) << "[" << sender << "] " << message;
}

void Logger::fatal(const std::string& sender, const std::string& message)
{
    BOOST_LOG_TRIVIAL(fatal) << "[" << sender << "] " << message;
}


