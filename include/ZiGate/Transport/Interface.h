/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <boost/signals2/signal.hpp>
#include <boost/system/error_code.hpp>
#include <memory>

#include "../Responses/DeviceAnnounce.h"
#include "../Responses/AttributeReport.h"

namespace ZiGate
{
    namespace Requests
    {
        class Base;
    }

    namespace Transport
    {
        /**
         * @brief Represents the ZiGate transport interface
         */
        class Interface
        {
          public:

            typedef boost::signals2::signal<void(const Responses::DeviceAnnounce&)> DeviceAnnounceSignal;
            typedef DeviceAnnounceSignal::slot_type DeviceAnnounceSlot;

            typedef boost::signals2::signal<void(const Responses::AttributeReport&)> AttributeReportSignal;
            typedef AttributeReportSignal::slot_type AttributeReportSlot;

            /**
             * @brief Destructor
             */
            virtual ~Interface() = default;

            /**
             * @brief Get information about the transport (.e.g. USB:/dev/serial0, Wifi:192.168.56.10:9999, etc.)
             * @return Information about the transport
             */
            [[nodiscard]] virtual std::string getInfo() const = 0;

            /**
             * @brief Open the communication
             * @return Information about the error if any
             */
            virtual boost::system::error_code open() = 0;

            /**
             * @brief Close the communication
             */
            virtual void close() = 0;

            /**
             * @brief Send a request
             * @param request The request to send
             * @return Information about the error if any
             */
            virtual boost::system::error_code send(const std::shared_ptr<Requests::Base>& request) = 0;

            /**
             * @brief Connect a slot to the DeviceAnnounce signal
             * @param slot The slot to connect
             * @return The connection between the signal and the slot
             */
            virtual boost::signals2::connection connectDeviceAnnounceSlot(const DeviceAnnounceSlot& slot) = 0;

            /**
             * @brief Connect a slot to the AttributeReport signal
             * @param slot The slot to connect
             * @return The connection between the signal and the slot
             */
            virtual boost::signals2::connection connectAttributeReportSlot(const AttributeReportSlot& slot) = 0;

          protected:

            /**
             * @brief Constructor
             */
            Interface() = default;
        };
    }
}