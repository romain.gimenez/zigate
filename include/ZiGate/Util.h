/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Typedefs.h"

namespace ZiGate::Util
{
    /**
     * @brief Get the hexadecimal string representation of a uint8 value
     * @param value The value to convert
     * @return The hexadecimal string representation of the value
     */
    [[nodiscard]] std::string uint8ToHexStr(std::uint8_t value);

    /**
     * @brief Get the hexadecimal string representation of a uint16 value
     * @param value The value to convert
     * @return The hexadecimal string representation of the value
     */
    [[nodiscard]] std::string uint16ToHexStr(std::uint16_t value);

    /**
     * @brief Get the hexadecimal string representation of buffer of bytes
     * @param value The value to convert
     * @return The hexadecimal string representation of the value
     */
    [[nodiscard]] std::string dataToHexStr(const ZiGate::data_t& value);

    /**
     * @brief Convert an hexadecimal string to a uint16 value
     * @param value The value to convert
     * @return The value converted into a uint16
     */
    [[nodiscard]] std::uint16_t hexStrToUint16(const std::string& value);
}
