/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Requests/SetSecurityKey.h"
#include "Message.h"

using namespace ZiGate::Requests;

SetSecurityKey::SetSecurityKey(std::uint8_t type, data_t data)
    : Base("SetSecurityKey", MessageType::SetSecurityKey, MessageType::Null)
    , m_type(type)
    , m_data(std::move(data))
{
    //ctor
}

ZiGate::Message SetSecurityKey::generate() const
{
    Message msg {static_cast<std::uint16_t>(m_commandType)};

    msg.write_uint8(m_type);
    msg.write_data(m_data);

    return msg;
}
