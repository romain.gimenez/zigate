/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace ZiGate::Constants
{
    const std::uint8_t ZERO_BYTE = 0x00u;
    const std::uint8_t START_BYTE = 0x01u;
    const std::uint8_t ENCODED_BYTE = 0x02u;
    const std::uint8_t STOP_BYTE = 0x03u;
    const std::uint8_t ENCODER_BYTE = 0x10u;
    const std::uint8_t START_SIZE = 1u;
    const std::uint8_t CMD_AT = START_SIZE;
    const std::uint8_t CMD_SIZE = 2u;
    const std::uint8_t LENGTH_AT = CMD_AT + CMD_SIZE;
    const std::uint8_t LENGTH_SIZE = 2u;
    const std::uint8_t CHKSM_AT = LENGTH_AT + LENGTH_SIZE;
    const std::uint8_t CHKSM_SIZE = 1u;
    const std::uint8_t DATA_AT = CHKSM_AT + CHKSM_SIZE;
    const std::uint8_t STOP_SIZE = 1u;

    const std::uint8_t MIN_SIZE = START_SIZE + CMD_SIZE + LENGTH_SIZE + CHKSM_SIZE + STOP_SIZE;
}
