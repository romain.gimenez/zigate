/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Logger.h"
#include "Message.h"
#include "Responses/ActiveEndpoints.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::ActiveEndpoints";
}

ActiveEndpoints::ActiveEndpoints(Message& message)
    : m_sequenceNumber(0u)
    , m_status(0u)
    , m_shortAddress(0u)
    , m_activeEndpoints()
{
    m_sequenceNumber = message.read_uint8();
    m_status = message.read_uint8();
    m_shortAddress = message.read_uint16();
    auto count = message.read_uint8();

    Logger::debug(LOG_SENDER, "Sequence number: " + std::to_string(m_sequenceNumber));
    Logger::debug(LOG_SENDER, "Status: " + std::to_string(m_status));
    Logger::debug(LOG_SENDER, "Short address: " + Util::uint16ToHexStr(m_shortAddress));
    Logger::debug(LOG_SENDER, "Endpoints count: " + std::to_string(count));

    for(std::uint8_t i = 0u; i < count; ++i)
    {
        m_activeEndpoints.emplace_back(message.read_uint8());
        Logger::debug(LOG_SENDER, " - Endpoint: " + Util::uint8ToHexStr(m_activeEndpoints.back()));
    }
}

std::uint8_t ActiveEndpoints::getSequenceNumber() const
{
    return m_sequenceNumber;
}

std::uint8_t ActiveEndpoints::getStatus() const
{
    return m_status;
}

std::uint16_t ActiveEndpoints::getShortAddress() const
{
    return m_shortAddress;
}

std::vector<std::uint8_t> ActiveEndpoints::getActiveEndpoints() const
{
    return m_activeEndpoints;
}
