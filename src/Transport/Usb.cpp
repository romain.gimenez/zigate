/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <boost/asio/placeholders.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/write.hpp>

#include "Logger.h"
#include "Message.h"
#include "Requests/Base.h"
#include "Transport/Usb.h"
#include "Util.h"

using namespace ZiGate::Transport;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Usb";
}

Usb::Usb(std::string path)
    : m_path(std::move(path))
    , m_context()
    , m_workGuard(m_context.get_executor())
    , m_port(m_context)
    , m_thread(std::bind(&Usb::run, this))
    , m_currentRequest(nullptr)
    , m_readBuffer()
    , m_mutex()
    , m_deviceAnnounceSignal()
    , m_attributeReportSignal()
{
    Logger::trace(LOG_SENDER, "Creating instance");

    // ctor

    Logger::trace(LOG_SENDER, "Instance created");
}

Usb::~Usb()
{
    Logger::trace(LOG_SENDER, "Deleting instance");

    m_deviceAnnounceSignal.disconnect_all_slots();
    m_attributeReportSignal.disconnect_all_slots();

    m_workGuard.reset();
    close();

    if (m_thread.joinable())
    {
        Logger::trace(LOG_SENDER, "Joining thread");

        m_thread.join();

        Logger::trace(LOG_SENDER, "Thread joined");
    }

    Logger::trace(LOG_SENDER, "Instance deleted");
}

std::string Usb::getInfo() const
{
    return "USB:" + m_path;
}

boost::system::error_code Usb::open()
{
    boost::system::error_code ec {};
    if (m_port.is_open())
    {
        Logger::warning(LOG_SENDER, "Communication already open");
        return ec;
    }

    Logger::info(LOG_SENDER, "Opening communication");

    m_port.open(m_path, ec);
    m_port.set_option(boost::asio::serial_port::baud_rate(115200), ec);
    m_port.set_option(boost::asio::serial_port::character_size(8), ec);
    m_port.set_option(boost::asio::serial_port::parity(boost::asio::serial_port::parity::none), ec);
    m_port.set_option(boost::asio::serial_port::stop_bits(boost::asio::serial_port::stop_bits::one), ec);

    if (!ec)
    {
        Logger::info(LOG_SENDER, "Communication opened");
        startReceiving();
    }
    else
    {
        Logger::error(LOG_SENDER, "Failed to open communication: " + ec.message());
    }

    return ec;
}

void Usb::close()
{
    if (!m_port.is_open())
    {
        Logger::info(LOG_SENDER, "Communication already closed");
        return;
    }

    Logger::info(LOG_SENDER, "Closing communication");

    m_port.close();

    Logger::info(LOG_SENDER, "Communication closed");
}

boost::system::error_code Usb::send(const std::shared_ptr<Requests::Base>& request)
{
    Logger::trace(LOG_SENDER, "Sending request");

    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_currentRequest)
        {
            return boost::system::error_code(static_cast<int>(boost::system::errc::device_or_resource_busy),
                                             boost::system::generic_category());
        }

        m_currentRequest = request;
    }

    boost::system::error_code ec {};
    boost::asio::write(m_port, boost::asio::buffer(request->generate().encode()), ec);

    if (ec)
    {
        Logger::error(LOG_SENDER, "Failed to send request: " + ec.message());
    }
    else
    {
        Logger::debug(LOG_SENDER, "Request sent");
    }

    return ec;
}

boost::signals2::connection Usb::connectDeviceAnnounceSlot(const Interface::DeviceAnnounceSlot& slot)
{
    return m_deviceAnnounceSignal.connect(slot);
}

boost::signals2::connection Usb::connectAttributeReportSlot(const Interface::AttributeReportSlot& slot)
{
    return m_attributeReportSignal.connect(slot);
}

void Usb::run()
{
    Logger::trace(LOG_SENDER, "Running");

    m_context.run();

    Logger::trace(LOG_SENDER, "Stopped");
}

void Usb::startReceiving()
{
    Logger::trace(LOG_SENDER, "Receiving");

    boost::asio::async_read_until(m_port, boost::asio::dynamic_buffer(m_readBuffer), Constants::STOP_BYTE,
                                  boost::bind(&Usb::handleReceive, this, boost::asio::placeholders::error,
                                              boost::asio::placeholders::bytes_transferred));
}

void Usb::handleReceive(const boost::system::error_code& error, std::size_t bytes_transferred)
{
    Logger::trace(LOG_SENDER, "Entering receive callback");

    if (error)
    {
        // Connection has been closed on our side = no error
        if (error == boost::system::errc::operation_canceled)
        {
            return;
        }

        // TODO better error handling / close the communication and restart ?
        Logger::error(LOG_SENDER, "Receive error: " + error.message());
        return;
    }

    auto message = Message::decode(data_t(m_readBuffer.begin(),
                                          m_readBuffer.begin() + bytes_transferred));

    // STATUS
    if (message.getType() == static_cast<std::uint16_t>(MessageType::Status))
    {
        Logger::debug(LOG_SENDER, "Received a status message");

        auto status = message.read_uint8();
        auto sequenceNumber = message.read_uint8(); // Sequence number, unused for now
        auto command = message.read_uint16();

        Logger::debug(LOG_SENDER, "Status: " + std::to_string(status));
        Logger::debug(LOG_SENDER, "Sequence number: " + std::to_string(sequenceNumber));
        Logger::debug(LOG_SENDER, "Command: " + Util::uint16ToHexStr(command));

        std::lock_guard<std::mutex> lock(m_mutex);
        if (!m_currentRequest || static_cast<std::uint16_t>(m_currentRequest->getCommandType()) != command)
        {
            Logger::warning(LOG_SENDER,
                            "Received a status message for an unknown request: " + Util::uint16ToHexStr(command));
        }
        else
        {
            m_currentRequest->handleStatus(status);

            if (m_currentRequest->getResponseType() == MessageType::Null)
            {
                m_currentRequest.reset();
                Logger::debug(LOG_SENDER, "Cleared current request because no response is expected");
            }
        }
    }
    else if (message.getType() ==
             static_cast<std::uint16_t>(MessageType::RouterDiscoveryConfirm)) // Router Discovery Confirm
    {
        auto status = message.read_uint8();
        auto networkStatus = message.read_uint8();

        Logger::info(LOG_SENDER, "RouterDiscoveryConfirm status(" + std::to_string(status) + "), network status(" +
                                 std::to_string(networkStatus) + ")");
    }
    else if (message.getType() ==
             static_cast<std::uint16_t>(MessageType::APSDataConfirmFail)) // APS Data Confirm Fail
    {
        auto status = message.read_uint8();
        auto srcEndpoint = message.read_uint8();
        auto dstEndpoint = message.read_uint8();
        auto dstAddrMode = message.read_uint8();
        auto dstAddr = message.read_uint16();
        auto seqNb = message.read_uint8();

        Logger::warning(LOG_SENDER, "APSDataConfirmFail status(" + std::to_string(status) + "), srcEndPoint(" +
                                    std::to_string(srcEndpoint) + ")" + "), dstEndPoint(" +
                                    std::to_string(dstEndpoint) + "), dstAddrMode(" +
                                    std::to_string(dstAddrMode) + "), dstAddr(" +
                                    Util::uint16ToHexStr(dstAddr) + "), seqNb(" +
                                    std::to_string(seqNb) + ")");
    }
    else if (message.getType() == static_cast<std::uint16_t>(MessageType::DeviceAnnounce)) // Device announce
    {
        m_deviceAnnounceSignal(Responses::DeviceAnnounce(message));
    }
    else if (message.getType() == static_cast<std::uint16_t>(MessageType::AttributeReport)) // Attribute report
    {
        m_attributeReportSignal(Responses::AttributeReport(message));
    }

    else // RESPONSE
    {
        Logger::debug(LOG_SENDER, "Received a response message");

        std::lock_guard<std::mutex> lock(m_mutex);
        if (!m_currentRequest || static_cast<std::uint16_t>(m_currentRequest->getResponseType()) != message.getType())
        {
            Logger::warning(LOG_SENDER, "Received a response message for an unknown request: " +
                                        Util::uint16ToHexStr(message.getType()));
        }
        else
        {
            m_currentRequest->handleResponseMessage(message);
            m_currentRequest.reset();
            Logger::debug(LOG_SENDER, "Clearing current request");
        }
    }

    Logger::debug(LOG_SENDER, "Bytes transferred: " + std::to_string(bytes_transferred));
    Logger::debug(LOG_SENDER, "Buffer size before cleanup: " + std::to_string(m_readBuffer.size()));

    // Erase data from the buffer and start receiving again
    m_readBuffer.erase(m_readBuffer.begin(), m_readBuffer.begin() + bytes_transferred);

    Logger::debug(LOG_SENDER, "Buffer size after cleanup: " + std::to_string(m_readBuffer.size()));

    startReceiving();
}
