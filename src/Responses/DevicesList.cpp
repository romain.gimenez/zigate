/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Devices/Info.h"
#include "Logger.h"
#include "Message.h"
#include "Responses/DevicesList.h"
#include "Util.h"

using namespace ZiGate::Responses;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Responses::DevicesList";
}

DevicesList::DevicesList(Message& message)
    : m_list()
{
    static const std::uint16_t DEVICE_DATA_SIZE = 13u;

    auto dataLength = message.getDataLength();
    Logger::debug(LOG_SENDER, "Data length: " + std::to_string(dataLength));

    std::uint16_t devicesCount = message.getDataLength() / DEVICE_DATA_SIZE;
    Logger::debug(LOG_SENDER, "Devices count: " + std::to_string(devicesCount));

    for(std::uint16_t i = 0; i < devicesCount; ++i)
    {
        auto id = message.read_uint8();
        auto shortAddress = message.read_uint16();
        auto ieeeAddress = message.read_uint64();
        auto acPower = message.read_bool();
        auto linkQuality = message.read_uint8();

        Logger::debug(LOG_SENDER, "ID: " + std::to_string(id));
        Logger::debug(LOG_SENDER, "Short address: " + Util::uint16ToHexStr(shortAddress));
        Logger::debug(LOG_SENDER, "IEEE address: " + std::to_string(ieeeAddress));
        Logger::debug(LOG_SENDER, "AC Power: " + std::to_string(acPower));
        Logger::debug(LOG_SENDER, "Link quality: " + std::to_string(linkQuality));

        m_list.emplace(id, Devices::Info(id, shortAddress, ieeeAddress, acPower, linkQuality));
    }
}

const DevicesList::List_t& DevicesList::getDevices() const
{
    return m_list;
}
