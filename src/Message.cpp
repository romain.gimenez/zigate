/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Logger.h"
#include "Message.h"
#include "Util.h"

using namespace ZiGate;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Message";

    data_t encode(const data_t& data)
    {
        data_t encoded(0u);
        encoded.reserve(data.size() * 2);

        for (const auto& byte : data)
        {
            if (byte > Constants::ENCODER_BYTE)
            {
                encoded.emplace_back(byte);
            }
            else
            {
                encoded.emplace_back(Constants::ENCODED_BYTE);
                encoded.emplace_back(byte ^ Constants::ENCODER_BYTE);
            }
        }
        return encoded;
    }

    data_t encode(std::uint16_t value)
    {
        data_t data(2);
        data[0] = static_cast<std::uint8_t>(value >> 8u);
        data[1] = static_cast<std::uint8_t>(value);
        return encode(data);
    }

    void append(data_t& buffer, const data_t& data)
    {
        buffer.insert(buffer.end(), data.begin(), data.end());
    }
}

Message::Message(std::uint16_t command)
    : m_command(command)
    , m_data()
    , m_readIndex(0)
{
    //ctor
}

Message::Message(std::uint16_t command, data_t data)
    : m_command(command)
    , m_data(std::move(data))
    , m_readIndex(0)
{
    //ctor
}

Message Message::decode(const data_t& encoded)
{
    Logger::debug(LOG_SENDER, "Encoded message: " + Util::dataToHexStr(encoded));

    data_t decoded(0u);
    decoded.reserve(encoded.size());

    bool flip = false;
    for (const auto& byte : encoded)
    {
        if (byte == Constants::ENCODED_BYTE)
        {
            flip = true;
        }
        else
        {
            if (flip)
            {
                decoded.emplace_back(byte ^ Constants::ENCODER_BYTE);
            }
            else
            {
                decoded.emplace_back(byte);
            }
            flip = false;
        }
    }

    Logger::debug(LOG_SENDER, "Decoded message: " + Util::dataToHexStr(decoded));

    auto command = static_cast<std::uint16_t>(decoded.at(Constants::CMD_AT) << 8u) |
                   static_cast<std::uint16_t>(decoded.at(Constants::CMD_AT + 1u));

    Logger::debug(LOG_SENDER, "MessageType: " + Util::uint16ToHexStr(command));

    auto length = static_cast<std::uint16_t>(decoded.at(Constants::LENGTH_AT) << 8u) |
                  static_cast<std::uint16_t>(decoded.at(Constants::LENGTH_AT + 1u));

    Logger::debug(LOG_SENDER, "Length: " + std::to_string(length));

    auto checksum = decoded.at(Constants::CHKSM_AT);

    Logger::debug(LOG_SENDER, "Checksum: " + Util::uint8ToHexStr(checksum));

    auto data = data_t(decoded.begin() + Constants::DATA_AT, decoded.begin() + Constants::DATA_AT + length);

    auto calculatedChecksum = calculateChecksum(command, length, data);
    Logger::debug(LOG_SENDER, "Calculated checksum: " + Util::uint8ToHexStr(calculatedChecksum));

    if (calculatedChecksum != checksum)
    {
        Logger::error(LOG_SENDER, "Malformed message");
    }

    return Message(command, data);
}

std::uint16_t Message::getType() const
{
    return m_command;
}

std::uint16_t Message::getDataLength() const
{
    return static_cast<std::uint16_t>(m_data.size());
}

data_t Message::encode() const
{
    data_t buffer;
    buffer.reserve((m_data.size() + Constants::MIN_SIZE) * 2u);

    Logger::debug(LOG_SENDER, "MessageType: " + Util::uint16ToHexStr(m_command));
    Logger::debug(LOG_SENDER, "Data: " + Util::dataToHexStr(m_data));

    buffer.emplace_back(Constants::START_BYTE);
    ::append(buffer, ::encode(m_command));
    ::append(buffer, ::encode(static_cast<std::uint16_t>(m_data.size())));

    buffer.emplace_back(calculateChecksum(m_command, static_cast<std::uint16_t>(m_data.size()), m_data));
    ::append(buffer, ::encode(m_data));

    buffer.emplace_back(Constants::STOP_BYTE);

    Logger::debug(LOG_SENDER, "Encoded: " + Util::dataToHexStr(buffer));

    return buffer;
}

void Message::resetRead()
{
    m_readIndex = 0u;
}

std::uint8_t Message::read_uint8()
{
    return m_data.at(m_readIndex++);
}

std::uint16_t Message::read_uint16()
{
    return static_cast<std::uint16_t>(read_uint8() << 8u) | static_cast<std::uint16_t>(read_uint8());
}

std::uint32_t Message::read_uint32()
{
    return static_cast<std::uint32_t>(read_uint16() << 16u) | static_cast<std::uint32_t>(read_uint16());
}

std::uint64_t Message::read_uint64()
{
    return static_cast<std::uint64_t>(read_uint32()) << 32u | static_cast<std::uint64_t>(read_uint32());
}

bool Message::read_bool()
{
    return (read_uint8() == 1u);
}

std::string Message::read_string(std::uint16_t size)
{
    auto data = read_data(size);
    return std::string(data.begin(), data.begin() + size);
}

data_t Message::read_data(std::uint16_t size)
{
    data_t result { m_data.begin() + m_readIndex,  m_data.begin() + m_readIndex + size};
    m_readIndex += size;
    return result;
}

void Message::write_uint8(std::uint8_t value)
{
    m_data.emplace_back(value);
}

void Message::write_uint16(std::uint16_t value)
{
    write_uint8(static_cast<std::uint8_t>(value >> 8u));
    write_uint8(static_cast<std::uint8_t>(value));
}

void Message::write_uint32(std::uint32_t value)
{
    write_uint16(static_cast<std::uint16_t>(value >> 16u));
    write_uint16(static_cast<std::uint16_t>(value));
}

void Message::write_uint64(std::uint64_t value)
{
    write_uint32(static_cast<std::uint32_t>(value >> 32u));
    write_uint32(static_cast<std::uint32_t>(value));
}

void Message::write_bool(bool value)
{
    write_uint8(value ? static_cast<std::uint8_t>(1u) : static_cast<std::uint8_t>(0u));
}

void Message::write_data(const data_t& value)
{
    m_data.insert(m_data.end(), value.begin(), value.end());
}

std::uint8_t Message::calculateChecksum(std::uint16_t command, std::uint16_t dataLength, const data_t& data)
{
    std::uint8_t result = Constants::ZERO_BYTE;

    result ^= static_cast<std::uint8_t>(command >> 8u);
    result ^= static_cast<std::uint8_t>(command);
    result ^= static_cast<std::uint8_t>(dataLength >> 8u);
    result ^= static_cast<std::uint8_t>(dataLength);

    for (const auto& byte : data)
    {
        result ^= byte;
    }

    return result;
}
