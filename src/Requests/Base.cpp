/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <thread>

#include "Requests/Base.h"
#include "Logger.h"
#include "Message.h"

using namespace ZiGate::Requests;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Requests::Base";
    const std::uint16_t WAIT_TIMEOUT = 3000u;
}

Base::Base(std::string name, MessageType commandType, MessageType responseType)
    : m_name(std::move(name))
    , m_status(Status::Created)
    , m_commandType(commandType)
    , m_responseType(responseType)
{
    //ctor
}

std::string Base::getName() const
{
    return m_name;
}

Base::Status Base::getStatus() const
{
    return m_status;
}

ZiGate::MessageType Base::getCommandType() const
{
    return m_commandType;
}

ZiGate::MessageType Base::getResponseType() const
{
    return m_responseType;
}

Base::Status Base::waitForCompletion()
{
    bool done;
    auto start = std::chrono::system_clock::now();
    do
    {
        auto status = m_status.load(std::memory_order::memory_order_release);
        done = (status == Status::Completed || status == Status::Failed);

        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now() - start).count();
        if (elapsed > WAIT_TIMEOUT)
        {
            m_status = Status::Timeout;
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    while (!done);

    return m_status;
}

ZiGate::Message Base::generate() const
{
    return ZiGate::Message(static_cast<std::uint16_t>(m_commandType));
}

void Base::handleStatus(std::uint8_t code)
{
    Logger::trace(LOG_SENDER, "Got status message: " + m_name);

    // Success
    if (code == 0u)
    {
        m_status = (m_responseType != MessageType::Null) ? Status::Received : Status::Completed;
        Logger::debug(LOG_SENDER, "Request received by the gateway");
    }
        // Error
    else
    {
        switch (code)
        {
            case 1u:
                Logger::error(LOG_SENDER, "Invalid parameter");
                break;
            case 2u:
                Logger::error(LOG_SENDER, "Unknown command");
                break;
            case 3u:
                Logger::error(LOG_SENDER, "Request failed");
                break;
            case 4u:
                Logger::error(LOG_SENDER, "Busy");
                break;
            case 5u:
                Logger::error(LOG_SENDER, "Stack already started");
                break;
            default:
                Logger::error(LOG_SENDER, "ZigBee error");
                break;
        }

        m_status = Status::Failed;
    }
}

void Base::handleResponseMessage(Message&)
{
    static const std::string error = "Unexpected response for command: ";
    Logger::error(LOG_SENDER, error + m_name);
}
