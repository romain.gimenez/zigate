/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <boost/asio/executor_work_guard.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/thread/thread.hpp>
#include <mutex>

#include "Interface.h"

namespace ZiGate::Transport
{
    /**
     * @brief Represents an USB transport
     */
    class Usb : public Interface
    {
      public:

        /**
         * @brief Constructor
         * @param path The path of the USB device (e.g. /dev/serial0)
         */
        explicit Usb(std::string path);

        /**
         * @brief Destructor
         */
        ~Usb() override;

        /**
         * @brief Get information about the transport (.e.g. USB:/dev/serial0, Wifi:192.168.56.10:9999, etc.)
         * @return Information about the transport
         */
        [[nodiscard]] std::string getInfo() const override;

        /**
         * @brief Open the communication with the device
         * @return Information about the error if any
         */
        boost::system::error_code open() override;

        /**
         * @brief Close the communication with the device
         */
        void close() override;

        /**
         * @brief Send a request
         * @param request The request to send
         * @return Information about the error if any
         */
        boost::system::error_code send(const std::shared_ptr<Requests::Base>& request) override;

        /**
         * @brief Connect a slot to the DeviceAnnounce signal
         * @param slot The slot to connect
         * @return The connection between the signal and the slot
         */
        boost::signals2::connection connectDeviceAnnounceSlot(const DeviceAnnounceSlot& slot) override;

        /**
         * @brief Connect a slot to the AttributeReport signal
         * @param slot The slot to connect
         * @return The connection between the signal and the slot
         */
        boost::signals2::connection connectAttributeReportSlot(const AttributeReportSlot& slot) override;

      private:

        /**
         * @brief Run the IO context
         */
        void run();

        /**
         * @brief Start asynchronous operation to receive data
         */
        void startReceiving();

        /**
         * @brief Callback the asynchronous receive operation
         * @param error Information about the error if any
         * @param bytes_transferred The number of bytes transferred
         */
        void handleReceive(const boost::system::error_code& error, std::size_t bytes_transferred);

        std::string m_path;
        boost::asio::io_context m_context;
        boost::asio::executor_work_guard<boost::asio::io_context::executor_type> m_workGuard;
        boost::asio::serial_port m_port;
        boost::thread m_thread;
        std::shared_ptr<Requests::Base> m_currentRequest;
        std::vector<std::uint8_t> m_readBuffer;
        mutable std::mutex m_mutex;
        DeviceAnnounceSignal m_deviceAnnounceSignal;
        AttributeReportSignal m_attributeReportSignal;
    };
}