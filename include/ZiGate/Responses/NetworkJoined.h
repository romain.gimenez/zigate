/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace ZiGate
{
    class Message;

    namespace Responses
    {
        class NetworkJoined
        {
          public:

            /**
             * @brief Constructor
             * @param message The message to parse for creating this response
             */
            explicit NetworkJoined(Message& message);

            /**
             * @brief Destructor
             */
            ~NetworkJoined() = default;

            /**
             * @brief Get status
             * @return 0 = Joined, 1 = Formed, 128-244 = ZigBee error
             */
            [[nodiscard]] std::uint8_t getStatus() const;

            /**
             * @brief Get the short address
             * @return The short address
             */
            [[nodiscard]] std::uint16_t getShortAddress() const;

            /**
             * @brief Get the extended address
             * @return The extended address
             */
            [[nodiscard]] std::uint64_t getExtendedAddress() const;

            /**
             * @brief Get the channel
             * @return The channel
             */
            [[nodiscard]] std::uint8_t getChannel() const;

          private:

            std::uint8_t m_status;
            std::uint16_t m_shortAddress;
            std::uint64_t m_extendedAddress;
            std::uint8_t m_channel;
        };
    }
}
