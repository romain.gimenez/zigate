/*
 * ZiGate
 * Copyright (c) 2019 Romain GIMENEZ
 *
 * This file is part of ZiGate.
 *
 * ZiGate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZiGate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZiGate.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Requests/GetVersion.h"
#include "Logger.h"
#include "Message.h"

using namespace ZiGate::Requests;

namespace
{
    const std::string LOG_SENDER = "ZiGate::Requests::GetVersion";
}

GetVersion::GetVersion()
    : Base("GetVersion", MessageType::GetVersion, MessageType::Version)
    , m_response(nullptr)
    , m_mutex()
{
    //ctor
}

const ZiGate::Responses::Version* GetVersion::getResponse() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_response.get();
}

void GetVersion::handleResponseMessage(Message& response)
{
    Logger::trace(LOG_SENDER, "Handling response message");

    std::lock_guard<std::mutex> lock(m_mutex);
    m_response = std::make_unique<Responses::Version>(response);

    m_status = Status::Completed;
}
